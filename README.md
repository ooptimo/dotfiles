# ooptimo's dot files

Alguns arxius de configuració que fem servir per als projectes d'ooptimo i que es poden utilizar com a punt de partida per a nous projectes o com a referència.

Es poden consultar o utilitzar directament, però hi ha alguns casos especials a tenir en compte:

1. `__generic___.htaccess`: S'ha de canviar de nom (treure'n el `__generic___`). Està pensat per a webs genèriques ja siguin html, php, etc
2. `__wordpresss___.htaccess`: S'ha de canviar de nom, treure'n el `__wordpresss___` i posar-lo a l'arrel d'una web feta amb Wordpress. A diferència del genèric, té algunes optimitzacions per a Wordpress.
3. `.yoast-seo-config`: No és pròpiament un _dotfile_ sinó que és una exportació d'una configuració genèrica del _plugin_ de Wordpress [Yoast SEO](https://yoast.com/). Per tant, no s'ha de copiar l'arxiu sinó que s'ha d'importar des de la secció `Yoast SEO > Herramientas > Importar y exportar` al panell d'administració del Wordpress.
